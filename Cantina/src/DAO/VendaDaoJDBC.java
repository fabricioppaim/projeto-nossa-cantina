/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import banco.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Venda;


public class VendaDaoJDBC implements VendaDAO {

    private Connection conexao;
    private PreparedStatement comando;
    @Override
    public void inserir(Venda venda) {
          String sql = "INSERT INTO Venda (matricula, cpfFuncionario, dtVenda, idProduto, valor, qtd_itens) "
                + "VALUES (?, ?, ?, ?, ?, ?)";
    
        try {
            iniciaConexao(sql);
            comando.setInt(1, venda.getId_aluno());
            comando.setInt(2, venda.getId_funcionario());
            comando.setInt(3, venda.getId_produto());
            comando.setDate(4, venda.getDataVenda());
            comando.setDouble(5, venda.getValor());
            comando.setInt(6, venda.getQtd_itens());
            comando.executeUpdate();
                    
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(VendaDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(VendaDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void deletar(Venda venda) {
    String sql = "DELETE FROM Venda WHERE idProduto=?";
        try {
            iniciaConexao(sql);
            comando.setInt(1, venda.getId_venda());
            comando.executeUpdate();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ProdutoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(ProdutoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public List<Venda> listar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Venda buscarPorId(int idVenda) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
        private void iniciaConexao(String sql) throws ClassNotFoundException, SQLException{
        
        conexao = ConnectionFactory.getConnection();
        comando = conexao.prepareStatement(sql);
    }
    
    private void fecharConexao() throws SQLException{
        
        comando.close();
        conexao.close();
    }
    
}

