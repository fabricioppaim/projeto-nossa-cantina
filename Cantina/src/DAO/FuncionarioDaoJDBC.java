/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import banco.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Funcionario;

/**
 *
 * @author 631220033
 */
public class FuncionarioDaoJDBC implements FuncionarioDAO{
    
    private Connection conexao;
    private PreparedStatement comando;
   
    
    @Override
    public void inserir(Funcionario funcionario) {
        String sql = "INSERT INTO Funcionario(cpfFuncionario, nome, usuario, senha) "
                + "VALUES (?, ?, ?, ?)";
        try {
            iniciaConexao(sql);
            comando.setLong(1, funcionario.getCPF());
            comando.setString(2, funcionario.getNome());
            comando.setString(3, funcionario.getUsuario());
            comando.setString(4, funcionario.getSenha());
            
            comando.executeUpdate();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(FuncionarioDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(FuncionarioDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }


    @Override
    public void deletar(Funcionario funcionario) {
       String sql = "DELETE FROM Funcionario WHERE cpfFuncionario=?";
        try {
            iniciaConexao(sql);
            comando.setLong(1, funcionario.getCPF());
            comando.executeUpdate();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(FuncionarioDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(FuncionarioDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    @Override
    public void atualizar(Funcionario funcionario) {
              String sql = "UPDATE Funcionario SET nome=?, senha=? "
                + "WHERE cpfFuncionario=?";
        try {
            iniciaConexao(sql);
            comando.setString(1, funcionario.getNome());
            comando.setString(2, funcionario.getSenha());
            comando.setLong(3, funcionario.getCPF());
            comando.executeUpdate();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(FuncionarioDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(FuncionarioDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }


    @Override
    public List<Funcionario> listar() {
                String sql = "SELECT * FROM Funcionario";
        List<Funcionario> listaFuncionarios = new ArrayList<>();
        try {
            iniciaConexao(sql);
            ResultSet resultado = comando.executeQuery();
            while(resultado.next())
            {
                Funcionario funcionario = new Funcionario(
                        resultado.getLong("cpf"),
                        resultado.getString("nome"),
                        resultado.getString("usuario"));
                listaFuncionarios.add(funcionario);
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(FuncionarioDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(FuncionarioDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listaFuncionarios;
    }

    @Override
    public Funcionario buscarPorId(long id) {
    String sql = "SELECT * FROM Funcionario WHERE cpffuncionario=?";
        Funcionario funcionario = null;
        try {
            iniciaConexao(sql);
            comando.setLong(1, id);
            ResultSet resultado = comando.executeQuery();
            while(resultado.next())
            {
                funcionario = new Funcionario(
                        resultado.getLong("cpffuncionario"),
                        resultado.getString("nome"),
                        resultado.getString("usuario"));
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(FuncionarioDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(FuncionarioDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return funcionario;
    }

    @Override
    public Funcionario buscarPorNome(String nome) {
        String sql = "SELECT * FROM Funcionario WHERE nome=?";
        Funcionario funcionario = null;
        try {
            iniciaConexao(sql);
            comando.setString(1, nome);
            ResultSet resultado = comando.executeQuery();
            while(resultado.next())
            {
                funcionario = new Funcionario(
                        resultado.getLong("cpffuncionario"),
                        resultado.getString("nome"),
                        resultado.getString("usuario"));
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(FuncionarioDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(FuncionarioDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return funcionario;
    }
    
    private void iniciaConexao(String sql) throws ClassNotFoundException, SQLException{
        
        conexao = ConnectionFactory.getConnection();
        comando = conexao.prepareStatement(sql);
    }
    
    private void fecharConexao() throws SQLException{
        
        comando.close();
        conexao.close();
    }
    
}
