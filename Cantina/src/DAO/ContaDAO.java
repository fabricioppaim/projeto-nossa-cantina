/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import java.util.List;
import model.Conta;

/**
 *
 * @author 181200302
 */
public interface ContaDAO {
    
    public void inserir(Conta conta);
    public void deletar(Conta conta);
    public void atualizar(Conta conta);
    public Conta checaConta(int num_conta);
    
}
