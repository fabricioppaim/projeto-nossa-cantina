/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import java.util.List;
import model.Funcionario;

/**
 *
 * @author 631220033
 */
public interface FuncionarioDAO {
    public void inserir(Funcionario funcionario);
    public void deletar(Funcionario funcionario);
    public void atualizar(Funcionario funcionario);
    public List<Funcionario> listar();
    public Funcionario buscarPorId(long cpfFuncionario);
    public Funcionario buscarPorNome(String nome);
}
