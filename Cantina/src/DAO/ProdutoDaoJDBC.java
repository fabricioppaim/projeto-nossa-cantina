/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import banco.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Produto;

/**
 *
 * @author 631220033
 */
public class ProdutoDaoJDBC implements ProdutoDAO{
    
    private Connection conexao;
    private PreparedStatement comando;
    
    @Override
    public void inserir(Produto produto) {
        String sql = "INSERT INTO Produto(nomeProduto, tipoProduto, precoProduto) "
                + "VALUES (?, ?, ?)";
        try {
            iniciaConexao(sql);
            comando.setString(1, produto.getNomeProduto());
            comando.setString(2, produto.getTipoProduto());
            comando.setDouble(3, produto.getPrecoProduto()); 
            comando.executeUpdate();
                    
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ProdutoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(ProdutoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void deletar(Produto produto) {
        String sql = "DELETE FROM Produto WHERE idProduto=?";
        try {
            iniciaConexao(sql);
            comando.setInt(1, produto.getIdProduto());
            comando.executeUpdate();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ProdutoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(ProdutoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void atualizar(Produto produto) {
                   String sql = "UPDATE Produto SET nomeProduto=?, tipoProduto=? precoProduto=?"
                + "WHERE idProduto=?";
        try {
            iniciaConexao(sql);
            comando.setString(1, produto.getNomeProduto());
            comando.setString(2, produto.getTipoProduto());
            comando.setDouble(3, produto.getPrecoProduto());
            comando.executeUpdate();
         
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ProdutoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(ProdutoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public List<Produto> listar() {
         String sql = "SELECT * FROM Produto";
        List<Produto> listaProdutos = new ArrayList<>();
        try {
            iniciaConexao(sql);
            ResultSet resultado = comando.executeQuery();
            while(resultado.next())
            {
               Produto produto = new Produto(
                        resultado.getInt("idProduto"),
                        resultado.getString("nomeProduto"),
                        resultado.getString("tipoProduto"),
                        resultado.getDouble("precoProduto"));
               
                listaProdutos.add(produto);
                
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ProdutoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(ProdutoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listaProdutos;
    }

    
    
    
    @Override
    public Produto buscarPorId(int id) {
        String sql = "SELECT * FROM Produto WHERE idProduto=?";
        Produto produto = null;
        try {
            iniciaConexao(sql);
            comando.setInt(1, id);
            ResultSet resultado = comando.executeQuery();
            while(resultado.next())
            {
                produto = new Produto(
                        resultado.getInt("idProduto"),
                        resultado.getString("nomeproduto"),
                        resultado.getString("tipoproduto"),
                        resultado.getDouble("precoproduto"));
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ProdutoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(ProdutoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return produto;
    }

   @Override
    public Produto buscarPorNome(String nome) {
    String sql = "SELECT * FROM produto WHERE nomeproduto=?";
        Produto produto = null;
        try {
            iniciaConexao(sql);
            comando.setString(1, nome);
            ResultSet resultado = comando.executeQuery();
            while(resultado.next())
            {
                produto = new Produto(
                        resultado.getInt("idProduto"),
                        resultado.getString("nomeproduto"),
                        resultado.getString("tipoproduto"),
                        resultado.getDouble("precoproduto"));
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ProdutoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(ProdutoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return produto;
    }
    
    private void iniciaConexao(String sql) throws ClassNotFoundException, SQLException{
        
        conexao = ConnectionFactory.getConnection();
        comando = conexao.prepareStatement(sql);
    }
    
    private void fecharConexao() throws SQLException{
        
        comando.close();
        conexao.close();
    }
    
}
