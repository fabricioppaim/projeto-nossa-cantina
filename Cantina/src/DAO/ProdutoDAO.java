/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import java.util.List;
import model.Produto;

/**
 *
 * @author 631220033
 */
public interface ProdutoDAO {
    
    public void inserir(Produto produto);
    public void deletar(Produto produto);
    public void atualizar(Produto produto);
    public List<Produto> listar();
    public Produto buscarPorId(int id);
    public Produto buscarPorNome(String nome);
}
