/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;
import model.Venda;

/**
 *
 * @author Rafael
 */
public interface VendaDAO {
    
    public void inserir(Venda venda);
    public void deletar(Venda venda);
    public void atualizar(Venda venda);
    public List<Venda> listar();
    public Venda buscarPorId(int idVenda);
}
