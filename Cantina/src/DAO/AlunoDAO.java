/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import java.util.List;
import model.Aluno;

/**
 *
 * @author 631220033
 */
public interface AlunoDAO {
                
    public void inserir(Aluno aluno);
    public void deletar(Aluno aluno);
    public void atualizar(Aluno aluno);
    public List<Aluno> listar();
    public Aluno buscarPorId(int matricula);
    public Aluno buscarPorNome(String nome);
    
}
