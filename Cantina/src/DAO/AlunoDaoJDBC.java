/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import banco.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Aluno;
import Interface.TelaPrincipal;
/**
 *
 * @author 631220033
 */
public class AlunoDaoJDBC implements AlunoDAO{
    
    private Connection conexao;
    private PreparedStatement comando;
       
    @Override
    public void inserir(Aluno aluno) {
        String sql = "INSERT INTO Aluno(matricula, nome, email, usuario, senha) "
                + "VALUES (?, ?, ?, ?, ?)";
        try {
            iniciaConexao(sql);
            comando.setInt(1, aluno.getMatricula());
            comando.setString(2, aluno.getNome());
            comando.setString(3, aluno.getEmail());
            comando.setString(4, aluno.getUsuario());
            comando.setInt(5, aluno.getSenha());
            
            comando.executeUpdate();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AlunoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(AlunoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    @Override
    public void deletar(Aluno aluno) {
        String sql = "DELETE FROM Aluno WHERE matricula=?";
        try {
            iniciaConexao(sql);
            comando.setInt(1, aluno.getId());
            comando.executeUpdate();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AlunoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(AlunoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    @Override
    public void atualizar(Aluno aluno) {
        String sql = "UPDATE Aluno SET nome=?, email=?, senha=? "
                + "WHERE matricula=?";
        try {
            iniciaConexao(sql);
            comando.setString(1, aluno.getNome());
            comando.setString(2, aluno.getEmail());
            comando.setInt(3, aluno.getMatricula());
            comando.executeUpdate();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AlunoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(AlunoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public List<Aluno> listar() {
        String sql = "SELECT * FROM Aluno order by nome";
        List<Aluno> listaAlunos = new ArrayList<>();
        try {
            iniciaConexao(sql);
            ResultSet resultado = comando.executeQuery();
            while(resultado.next())
            {
                Aluno aluno = new Aluno(
                        resultado.getInt("matricula"),
                        resultado.getString("nome"),
                        resultado.getString("email"));
                listaAlunos.add(aluno);
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AlunoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(AlunoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listaAlunos;
    }
    
    @Override
    public Aluno buscarPorId(int matricula) {
        String sql = "SELECT * FROM Aluno WHERE matricula=?";
        Aluno aluno = null;
        try {
            iniciaConexao(sql);
            comando.setInt(1, matricula);
            ResultSet resultado = comando.executeQuery();
            
            while(resultado.next())
            {
                aluno = new Aluno(
                        resultado.getInt("matricula"),
                        resultado.getString("nome"),
                        resultado.getString("email"));
                
                System.out.println(aluno.getNome()+ "Resultado 1 ");
                
                
            }
                        
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AlunoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(AlunoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return aluno;
    }

    @Override
    
    public Aluno buscarPorNome(String nome) {
        System.out.println("Entrou na Busca por nome");
        
        String sql = "SELECT * FROM Aluno WHERE nome=?";
        Aluno aluno = null;
        try {
            
            iniciaConexao(sql);
            comando.setString(1, nome);
            ResultSet resultado = comando.executeQuery();
            while(resultado.next())
            {
                aluno = new Aluno(
                        resultado.getInt("matricula"),
                        resultado.getString("nome"),
                        resultado.getString("email"));
                
                
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(AlunoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(AlunoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return aluno;
    }
    
    private void iniciaConexao(String sql) throws ClassNotFoundException, SQLException{
        
        conexao = ConnectionFactory.getConnection();
        comando = conexao.prepareStatement(sql);
    }
    
    private void fecharConexao() throws SQLException{
        
        comando.close();
        conexao.close();
    }


}
