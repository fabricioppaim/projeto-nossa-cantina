/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import Interface.TelaPrincipal;
import banco.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Conta;


public class ContaDaoJDBC implements ContaDAO {
    
       private Connection conexao;
    private PreparedStatement comando;

    @Override
    public void inserir(Conta conta) {
             String sql = "INSERT INTO Conta(num_conta, saldo, bloqueada, premium) "
                + "VALUES (?, ?, ?, ?)";
        try {
            iniciaConexao(sql);
            comando.setInt(1, conta.getNum_conta());
            comando.setDouble(2, conta.getSaldo());
            comando.setBoolean(3, false);
            comando.setBoolean(4, false);
            
            comando.executeUpdate();
                    
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ContaDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(ContaDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    @Override
    public void deletar(Conta conta) {
       String sql = "DELETE FROM Conta WHERE num_conta=?";
        try {
            iniciaConexao(sql);
            comando.setInt(1, conta.getNum_conta());
            comando.executeUpdate();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ContaDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(ContaDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    @Override
    public void atualizar(Conta conta) {
        String sql = "UPDATE Conta SET saldo=? WHERE num_conta=?";
        try {
            iniciaConexao(sql);
            comando.setDouble(1, conta.getSaldo());
            comando.setInt(2, conta.getNum_conta());            
//          comando.setBoolean(3, conta.isBloqueada());
//          comando.setBoolean(4, conta.isPremium());
            comando.executeUpdate();
         
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ContaDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(ContaDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }


    @Override
    public Conta checaConta(int num_conta) {
            String sql = "SELECT * FROM Conta WHERE num_conta=?";
        Conta conta = null;
        try {
            iniciaConexao(sql);
            comando.setInt(1, num_conta);
            ResultSet resultado = comando.executeQuery();
            while(resultado.next())
            {
                conta = new Conta(
                        resultado.getInt("num_conta"),
                        resultado.getDouble("saldo"),
                        resultado.getBoolean("bloqueada"),
                        resultado.getBoolean("premium"));
            }
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ProdutoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fecharConexao();
            } catch (SQLException ex) {
                Logger.getLogger(ProdutoDaoJDBC.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return conta;
    }
    private void iniciaConexao(String sql) throws ClassNotFoundException, SQLException{
        
        conexao = ConnectionFactory.getConnection();
        comando = conexao.prepareStatement(sql);
    }
    
    private void fecharConexao() throws SQLException{
        
        comando.close();
        conexao.close();
    }
    
}
