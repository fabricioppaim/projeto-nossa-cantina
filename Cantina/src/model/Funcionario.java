/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author 631220033
 */
public class Funcionario {
    
    private long CPF;
    private String nome;
    private String usuario;
    private String senha;

    public Funcionario(long CPF, String nome, String usuario) {
        this.CPF = CPF;
        this.nome = nome;
        this.usuario = usuario;
    }
    
    public Funcionario(long CPF, String nome, String usuario, String senha) {
        this.CPF = CPF;
        this.nome = nome;
        this.usuario = usuario;
        this.senha = senha;
    }

    public Funcionario(long CPF) {
        this.CPF = CPF;
    }
    public Funcionario() {
        
    }
     
    /**
     * @return the CPF
     */
    public long getCPF() {
        return CPF;
    }

    /**
     * @param CPF the CPF to set
     */
    public void setCPF(long CPF) {
        this.CPF = CPF;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }
}
