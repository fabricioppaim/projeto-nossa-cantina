/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author 181200302
 */
public class Conta {
    
    private int num_conta;
    private double saldo;
    private boolean premium;
    private boolean bloqueada;
    private double valor;

    public Conta(int num_conta) {
    this.num_conta = num_conta;
             
    }

    public Conta(double saldo) {
        this.saldo = saldo;
    }

    public Conta(int num_conta, double saldo, boolean premium, boolean bloqueada) {
        this.num_conta = num_conta;
        this.saldo = saldo;
        this.premium = premium;
        this.bloqueada = bloqueada;
        System.out.println("####Verifica se os valores da CONTA foram armazenados no id, conta: "
                     + num_conta + " salso:  " + saldo );

    }

    public Conta() {
    }
    
    
    
    public int getNum_conta() {
        return num_conta;
    }

    public void setNum_conta(int num_conta) {
        this.num_conta = num_conta;
    }

    public double getSaldo() {
        return saldo;
    }

    protected void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public boolean isPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }

    public boolean isBloqueada() {
        return bloqueada;
    }

    public void setBloqueada(boolean bloqueada) {
        this.bloqueada = bloqueada;
    }
    
    public void depositar(Double valor) {
        this.saldo += valor;
    }
    
    public void descontar(Double valor){
        this.saldo = valor;
    }

    public boolean transferir() {
        if (getSaldo() < valor) {
            return false;
        } else {
            setSaldo(getSaldo() - valor);
            return true;
        }
    }
}
