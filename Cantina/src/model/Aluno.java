/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author 631220033
 */
public class Aluno {
    
    private Conta conta;
    private int matricula;
    private String email;
    private String nome;
    private String usuario;
    private int senha;

    public Aluno() {
    }
        
    public Aluno(String nome) {
        this.nome = nome;
        System.out.println("Verifica se os valores foram armazenados nome");
    }
    
    public Aluno(int id) {
         this.matricula = id;
         System.out.println("Verifica se os valores foram armazenados id");
    }
    
    public Aluno(String nome, String email) {
        this.nome = nome;
        this.email = email;
        System.out.println("Verifica se os valores foram armazenados nome e e-mail");
    }
    
    public Aluno(int id, String nome, String email) {
        this.matricula = id;
        this.nome = nome;
        this.email = email;
        System.out.println("Verifica se os valores ALUNO foram armazenados id, nome e e-mail: " + nome + ", Matricula: " + matricula);
    }
    public Aluno(int id, String nome, String email, String usuario, int senha) {
        this.matricula = id;
        this.nome = nome;
        this.email = email;
        this.usuario = usuario;
        this.senha = senha;
        System.out.println("Verifica se os valores foram armazenados todos campos");    
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }
  
    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the senha
     */
    public int getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(int senha) {
        this.senha = senha;
    }

    /**
     * @return the id
     */
    public int getId() {
        return matricula;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.matricula = id;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }
}
