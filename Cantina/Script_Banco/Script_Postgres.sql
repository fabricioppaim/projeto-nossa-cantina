
CREATE TABLE Aluno(
matricula INT,
nome VARCHAR(50),
email VARCHAR (50),
usuario VARCHAR(10),
senha VARCHAR(8),
CONSTRAINT pk_idAluno PRIMARY KEY (matricula)
)

CREATE TABLE Funcionario(
cpfFuncionario DECIMAL(11),
nome VARCHAR(50),
usuario VARCHAR(10),
senha VARCHAR(8),
CONSTRAINT pk_idFuncionario PRIMARY KEY (cpfFuncionario)
)

CREATE TABLE Produto(
idProduto SERIAL,
nomeProduto VARCHAR(50),
tipoProduto VARCHAR(50),
precoProduto DECIMAL(3,2),
CONSTRAINT pk_idProduto PRIMARY KEY (idProduto)
)

CREATE TABLE Venda(
idVenda SERIAL,
matricula INT REFERENCES Aluno(matricula),
cpfFuncionario INT REFERENCES Funcionario(cpfFuncionario),
dtVenda DATE,
idProduto INT REFERENCES Produto (idProduto),
valor NUMERIC,
qtd_itens INT,
CONSTRAINT pk_idVenda PRIMARY KEY (idVenda)
)

CREATE TABLE Conta(
num_conta INT REFERENCES Aluno(matricula),
saldo DECIMAL,
bloqueada VARCHAR,
premium VARCHAR,
cpfFuncionario INT REFERENCES Funcionario(cpfFuncionario)
)

